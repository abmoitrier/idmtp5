#include <iostream>
#include <string>
#include <future>
#include <chrono>
#include <fstream>
#include <cmath>
#include "CLHEP/Random/MTwistEngine.h"


/**
 * @brief Cette méthode génère 5 fois 10 nombres pseudo-aléatoires à partir d'un générateur Mersenne Twister et créé des fichiers de statut après chaque réplication.
 * Elle affiche également les nombres générés en sortie standard.
 * 
 * @param mtRng Générateur Mersenne Twister
 */
void sauvStatus(CLHEP::MTwistEngine *mtRng) {
  std::string filename;
  double rand;
  for (int k = 0; k < 5; k++) {
    filename = "status" + std::to_string(k) + ".dat";
    mtRng->saveStatus(filename.c_str()); // Sauvegarder l'état du générateur
    for (int i = 0; i < 10; i++) {
      rand = mtRng->flat(); // Générer un nombre aléatoire
      std::cout << rand << " ";
    }
    std::cout << std::endl;
  }
}

/**
 * @brief Cette méthode génère 5 fois 10 nombres pseudo-aléatoires à partir d'un générateur Mersenne Twister et de fichiers de statut pour chaque réplication.
 * Elle affiche également les nombres générés en sortie standard.
 * 
 * @param mtRng Générateur Mesernne Twister
 */
void restStatus(CLHEP::MTwistEngine *mtRng) {

  std::string filename;
  double rand;
  for (int k = 0; k < 5; k++) {
    filename = "status" + std::to_string(k) + ".dat";
    mtRng->restoreStatus(filename.c_str()); // Restaurer l'état du générateur
    for (int i = 0; i < 10; i++) {
      rand = mtRng->flat(); // Générer un nombre aléatoire
      std::cout << rand << " ";
    }
    std::cout << std::endl;
  }
}

/**
 * @brief Il s'agit d'une combinaison de la méthode1 et 2 pour vérifier que les nombres générés sont identiques.
 * 
 * @param mtRng Générateur Mesernne Twister
 */
void question2(CLHEP::MTwistEngine *mtRng)
{
  sauvStatus(mtRng);
  std::cout << std::endl;
  restStatus(mtRng);

}

/**
 * @brief Sauvegarde de 10 statuts tous les 2 milliards de nombres tirés.
 * 
 * @param mtRng Générateur Mesernne Twister
 */
void question3(CLHEP::MTwistEngine *mtRng)
{
  std::string filename;
  double rand;
  for (int k = 0; k < 10; k++) {
    filename = "status_question3_" + std::to_string(k) + ".dat";
    mtRng->saveStatus(filename.c_str()); // Sauvegarder l'état du générateur
    for (int i = 0; i < 2000000000; i++) {
      rand = mtRng->flat(); // Générer un nombre aléatoire
    }
  }
}

/**
 * @brief Cette méthode recherche une approximation de PI grâce à la méthode de Monte Carlo avec 1 milliards de points tirés. Le tout en utilisant un générateur Mersenne Twister.
 * 
 * @param mtRng Générateur Mesernne Twister
 * @return double Approximation de PI
 */
double methode_pi(CLHEP::MTwistEngine *mtRng){
  int count = 0;
  double randx;
  double randy;
  for (int i = 0; i < 1000000000; i++) {
    randx = mtRng->flat(); // Générer un nombre aléatoire
    randy = mtRng->flat(); // Générer un nombre aléatoire
    if(randx*randx + randy*randy < 1)
    {
      count++;
    }
  }
  return 4.0 * count/1000000000;

}

/**
 * @brief Génération séquentielle de 10 approximations de PI à partir de statuts sauvegardés et affichage de ces derniers dans la sortie standard.
 * 
 * @param mtRng Générateur Mesernne Twister
 */
void question4(CLHEP::MTwistEngine *mtRng)
{
  std::string filename;
  double pi[10];
  for (int k = 0; k < 10; k++) {
    filename = "status_question3_" + std::to_string(k) + ".dat";
    mtRng->restoreStatus(filename.c_str()); // Restaurer l'état du générateur
    pi[k] = methode_pi(mtRng);
    std::cout << pi[k] << std::endl;
  }
}

void question5()
{
  std::string filename;
  std::future<double> fut[10];
  CLHEP::MTwistEngine * mtRngs[10];
  double pi[10];
  for (int k = 0; k < 10; k++) {
    mtRngs[k] = new CLHEP::MTwistEngine();
    filename = "status_question3_" + std::to_string(k) + ".dat";
    mtRngs[k]->restoreStatus(filename.c_str()); // Sauvegarder l'état du générateur
    fut[k] = std::async(std::launch::async, methode_pi, mtRngs[k]);
  }
  for(int i = 0; i<10; i++){
    pi[i] = fut[i].get();
    std::cout << pi[i] << std::endl;
  }
}

/**
 * @brief Cette méthode calcule le nombre d'essais permettant de trouver une séquence nucléique donnée puis l'affiche en sortie standard.
 * 
 * @param mtRng Générateur Mesernne Twister
 */
void question6b(CLHEP::MTwistEngine *mtRng)
{
  double rand;
  std::string chaine_nucleique;
  std::string chaine_attendue = "AAATTTGCGTTCGATTAG";
  long nbEssais = 0;
  int longueur_chaine = chaine_attendue.length();
  while(chaine_nucleique != chaine_attendue){
    chaine_nucleique = "";
    for(int i = 0; i < longueur_chaine ; i++){
      rand = mtRng->flat();
      if(rand < 0.25){
        chaine_nucleique += "A";
      }
      else if (rand < 0.5){
        chaine_nucleique += "T";
      }
      else if (rand < 0.75){
        chaine_nucleique += "G";
      }
      else{
        chaine_nucleique += "C";
      }
      if(chaine_nucleique[i] != chaine_attendue[i])
      {
        break;
      }
    }
    nbEssais++;
    if(nbEssais%1000000000 == 0){
      std::cout << chaine_nucleique << "\n";
    }
  }
  std::cout << nbEssais << std::endl;
  std::cout << chaine_nucleique << std::endl;
}

/**
 * @brief Cette méthode réalise 40 réplications dans lesquelles, elle cherche la première apparition d'une certaine chaîne nucléique donnée.
 * Elle sauvegarde également le nombre d'essais avant d'y parvenir pour chaque réplication. Enfin, elle affiche dans la sortie standard quelques informations pour le débuggage.
 * 
 * @param mtRng Générateur Mesernne Twister
 */

void question6bplusfichier(CLHEP::MTwistEngine *mtRng)
{
  std::ofstream monFichier("nbEssais.txt");
  if (!monFichier) { // Vérification de l'ouverture du fichier
    std::cout << "Erreur : impossible d'ouvrir le fichier." << std::endl;
   }
  double rand;
  std::string filename[40];
  for(int k = 0; k< 40; k++){
    filename[k] = "reussite_" + std::to_string(k) + ".dat";
  }
  std::string chaine_nucleique;
  std::string chaine_attendue = "AAATTTGCGTTCGATTAG";
  long nbEssais = 0;
  int longueur_chaine = chaine_attendue.length();
  for(int k = 0; k < 40; k++){
    auto start = std::chrono::high_resolution_clock::now();
    mtRng->saveStatus(filename[k].c_str());
    while(chaine_nucleique != chaine_attendue){
      chaine_nucleique = "";
      for(int i = 0; i < longueur_chaine ; i++){
        rand = mtRng->flat();
        if(rand < 0.25){
          chaine_nucleique += "A";
        }
        else if (rand < 0.5){
          chaine_nucleique += "T";
        }
        else if (rand < 0.75){
          chaine_nucleique += "G";
        }
        else{
          chaine_nucleique += "C";
        }
        if(chaine_nucleique[i] != chaine_attendue[i])
        {
          break;
        }
      }
      nbEssais++;
      if(nbEssais%1000000000 == 0){
        std::cout << chaine_nucleique << "\n";
      }
    }
    std::cout << nbEssais << std::endl;
    monFichier << "reussite_" << k << " : " << nbEssais << std::endl; // Écriture dans le fichier
    std::cout << chaine_nucleique << std::endl;
    nbEssais = 0;
    chaine_nucleique = "";
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
    std::cout << "Durée d'exécution : " << duration.count() << " secondes" << std::endl;
    monFichier << "Durée d'exécution : " << duration.count() << " secondes" << std::endl;
  }
  monFichier.close();
}

/**
 * @brief Cette méthode sert à reprendre les tirages après un certain statut.
 * 
 * @param mtRng Générateur Mesernne Twister
 */
void question6bplusfichierbis(CLHEP::MTwistEngine *mtRng)
{
  std::ofstream monFichier("nbEssais4.txt");
  if (!monFichier) { // Vérification de l'ouverture du fichier
    std::cout << "Erreur : impossible d'ouvrir le fichier." << std::endl;
   }
  double rand;
  std::string filename[40];
  for(int k = 0; k< 40; k++){
    filename[k] = "reussite_" + std::to_string(k) + ".dat";
  }
  mtRng->restoreStatus("reussite_36.dat");
  std::string chaine_nucleique;
  std::string chaine_attendue = "AAATTTGCGTTCGATTAG";
  long nbEssais = 0;
  int longueur_chaine = chaine_attendue.length();
  for(int k = 36 ; k < 40; k++){
    auto start = std::chrono::high_resolution_clock::now();
    mtRng->saveStatus(filename[k].c_str());
    while(chaine_nucleique != chaine_attendue){
      chaine_nucleique = "";
      for(int i = 0; i < longueur_chaine ; i++){
        rand = mtRng->flat();
        if(rand < 0.25){
          chaine_nucleique += "A";
        }
        else if (rand < 0.5){
          chaine_nucleique += "T";
        }
        else if (rand < 0.75){
          chaine_nucleique += "G";
        }
        else{
          chaine_nucleique += "C";
        }
        if(chaine_nucleique[i] != chaine_attendue[i])
        {
          break;
        }
      }
      nbEssais++;
      if(nbEssais%1000000000 == 0){
        std::cout << chaine_nucleique << "\n";
      }
    }
    std::cout << nbEssais << std::endl;
    monFichier << "reussite_" << k << " : " << nbEssais << std::endl; // Écriture dans le fichier
    std::cout << chaine_nucleique << std::endl;
    nbEssais = 0;
    chaine_nucleique = "";
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
    std::cout << "Durée d'exécution : " << duration.count() << " secondes" << std::endl;
    monFichier << "Durée d'exécution : " << duration.count() << " secondes" << std::endl;
  }
  monFichier.close();
}

/**
 * @brief Méthode calculant l'écart-type avec des données brutes.
 * 
 */
void calculEcarttype(){
  long tab[] = {21666763479 , 29904817097 , 120772967063 , 126676924310, 91296664811,13272162313,64188410554,116338115813,45449143993,4736789700,34644060628,68586604952,9144570147,107833254365,11351781695,86686184728,11015365661,38820226837,81912835244,59328584852,127049066165,17892491081,9732905294,36077913005,49038091916,59470203811,4590863096,114959402133,155433400260,67813420504,101281608863,18765763375,8936855853,51014041213,147332195790,38287201114,18914794473,4072646826,33785535051,360473785989};
  double moy = 64213710351.35; // Moyenne calculée avec une calculette
  double moycalc = 0; 
  double somme = 0;
  int nombrereplic = 40;
  for(int i = 0; i< nombrereplic; i++){
    moycalc +=tab[i];
  }
  moycalc/= nombrereplic;
  std::cout<< moycalc << "\n";
  for(int i = 0; i< nombrereplic; i++){
    somme+=(tab[i] - moycalc)*(tab[i] - moycalc);
  }
  somme/=nombrereplic;
  std::cout << somme << "\n";
  somme = sqrt(somme);
  std::cout << "L'écart-type est de : " << somme << "\n";
}


//questions à décommenter et recommenter en fonction de ce que l'on veut lancer
int main() 
{
  auto start = std::chrono::high_resolution_clock::now();
  CLHEP::MTwistEngine *mtRng = new CLHEP::MTwistEngine();
  question2(mtRng);
  //question3(mtRng);
  // question4(mtRng);
  // question5();
  // question6b(mtRng);
  // question6bplusfichierbis(mtRng);
  // calculEcarttype();
  delete mtRng;
  auto stop = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);
  std::cout << "Durée d'exécution totale : " << duration.count() << " secondes" << std::endl;
  return 0;
}

